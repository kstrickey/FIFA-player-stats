"""
ADAPTED FROM A SCRIPT WRITTEN FOR ALLESINA LAB: Original created on Wed Jul 12 13:59:00 2017


CODE USED TO SCRAPE DATA FROM fifaindex.com

@author: kevin
"""

import urllib.parse
import urllib.request
from urllib.error import HTTPError
import re
import pandas as pd
import ssl
import time


# url segments to access all fifa versions available. For fifa versions with multiple dates,
# the one closest to Sept. 1 of the previous year (e.g. for FIFA 13, the closest one to Sept. 1, 2012) was chosen.
fifa_versions = [
    'fifa05_1',
    'fifa06_2',
    'fifa07_3',
    'fifa08_4',
    'fifa09_5',
    'fifa10_6',
    'fifa11_7',
    'fifa12_8',
    'fifa13_11',
    'fifa14_12',
    'fifa15_16',
    'fifa16_19',
    'fifa17_74',
    'fifa18_174'
]

def getlist(fifa_version, page_num):
    url = 'https://www.fifaindex.com/players/' + fifa_version + '/' + str(page_num) + '/'

    req = urllib.request.Request(url)

    # req.add_header('Cookie', '__cfduid=d3b816325b7ec0df05b9e31c2a94125f31499884652')
    # req.add_header('Cookie', '_ga=GA1.2.1714702311.1499884654')
    # req.add_header('Cookie', '_gid=GA1.2.605200764.1499884654')
    # req.add_header('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8')
    req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0')

    context = ssl._create_unverified_context()

    with urllib.request.urlopen(req, context=context) as f:
        source = f.read().decode('utf-8')

    source = urllib.parse.unquote(source)
    # print(source)

    playerstringpattern = re.compile('<tr data-playerid=(.*?)</tr>')
    playerstrings = re.findall(playerstringpattern, source)

    playerdata = []

    withinplayerpattern = re.compile('<td><a href=\\\"/player/([0-9]*)/(.*?)/fifa.*?/\\\" title=\\\"(.*?)\\\" class=\\\"link-player\\\"><img.*?\\\"player small\\\" /></a></td><td data-title=\\\"Nationality\\\">.*?title=\\\"(.*?)\\\".*?\\\"OVR / POT\\\"><span.*?>([2-9][0-9])</span><span.*?>([2-9][0-9])</span></td>.*?Preferred Positions\\\".*?>((?:<a.*?><span.*?>[A-Z]+</span></a>)+?)</td><td data-title=\\\"Age\\\">([0-9]+?)</td>.*?Hits\\\">.*?</td><td data-title=\\\"Team\\\">(?:<a.*?title=\\\"(.*?)\\\")?')

    for ps in playerstrings:
        # print(ps)
        player_info = list(re.findall(withinplayerpattern, ps)[0])

        # Players may or may not play multiple positions, so we still need to separate these out (store as a list)
        positions_str = player_info[6]
        pospatt = re.compile('<a.*?><span.*?>([A-Z]+?)</span></a>')
        positions = re.findall(pospatt, positions_str)
        primarypos = positions.pop(0)
        player_info[6] = primarypos

        otherpositions = '/'.join(positions)
        player_info.insert(7, otherpositions)

        # Convert ratings and age to numerics
        player_info[4] = int(player_info[4])
        player_info[5] = int(player_info[5])
        player_info[8] = int(player_info[8])

        playerdata.append(player_info)

    return playerdata


# print(getlist('fifa15_1', 197))

HEADERS = ['NumericID', 'StrippedName', 'Name', 'Nationality', 'OVR', 'POT', 'PrimaryPosition', 'OtherPositions', 'Age', 'Team']

# quit()
# Loop through FIFA versions: One file per FIFA version

for fifa_version in fifa_versions:

    print('Beginning to scrape player info from FIFA version ' + fifa_version)

    all_pages_list = []

    # Loop through pages of players

    page = 1
    while True:
        try:
            page_data_list = getlist(fifa_version, page)
        except HTTPError:
            print('\n****\nFinished scraping fifa version ' + fifa_version + ' after ' + str(page-1) + ' pages')
            break

        all_pages_list.extend(page_data_list)

        print('Finished page ' + str(page) + ', all_pages_list now has ' + str(len(all_pages_list)) + ' players')

        page += 1
        time.sleep(2.5)

    # Save to csv using pandas dataframe
    df = pd.DataFrame(all_pages_list, columns=HEADERS)
    df.to_csv('scraped player data/player_data_version_' + fifa_version + '.csv')

    print('\n\n=========\nWritten to CSV for version ' + fifa_version + '\n\n')

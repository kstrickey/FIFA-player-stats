#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 12 13:59:00 2017


CODE USED TO SCRAPE DATA FROM fifaindex.com

@author: kevin
"""

import urllib.request
from urllib.error import HTTPError
import re
import unicodecsv as csv




def getlist(league, page):
    url = 'https://www.fifaindex.com/players/' + str(page) + '/?league=' + str(league)
    
    req = urllib.request.Request(url)
    
    req.add_header('Cookie', '__cfduid=d3b816325b7ec0df05b9e31c2a94125f31499884652')
    req.add_header('Cookie', '_ga=GA1.2.1714702311.1499884654')
    req.add_header('Cookie', '_gid=GA1.2.605200764.1499884654')
    req.add_header('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8')
    req.add_header('User-Agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:54.0) Gecko/20100101 Firefox/54.0')
    
    with urllib.request.urlopen(req) as f:
        source = f.read().decode('utf8')
    
#    print(source)
    
    playerstringpattern = re.compile('<tr\\ data-playerid=(.*?)</tr>')
    playerstrings = re.findall(playerstringpattern, source)
    
#    print(playerstrings)

    playerdata = []

    withinplayerpattern = re.compile('<td><a\\ href=\\\"/player/[0-9]*/.*?/\\\"\\ title=\\\"(.*?)\\\">.*?<td\\ data\\-title=\\\"Nationality\\\"><a\\ href=.*?title=\\\"(.*?)\\\">.*?<td\\ data\\-title=\\\"OVR\\ /\\ POT\\\"><span\\ class=\\\"label\\ rating\\ r[0-9]\\\">([0-9]*)</span><span\\ class=\\\"label\\ rating\\ r[0-9]\\\">([0-9]*)</span></td>.*?Preferred\\ Positions.*?<span\\ class=.*?>([A-Z]*?)</span></a>.*?<td\\ data\\-title=\\\"Age\\\">([0-9]*?)</td>.*?<td\\ data\\-title=\\\"Team\\\"><a\\ href.*?\\ title=\\\"(.*?)\\\">')

    for ps in playerstrings:
        playerdata.extend(re.findall(withinplayerpattern, ps))
    
    return playerdata
    

league_numbers = {350: 'ALG League',
                  1: 'Alka Superliga',
                  56: 'Allsvenskan',
                  13: 'Barclays PL',
                  19: 'Bundesliga',
                  20: 'Bundesliga 2',
                  335: 'Camp. Scotiabank',
                  66: 'Ekstraklasa',
                  10: 'Eredivisie',
                  14: 'FL Championship',
                  60: 'Football League 1',
                  61: 'Football League 2',
                  382: 'Free Agents',
                  351: 'Hyundai A-league',
                  349: 'Japan J1 League (1)',
                  83: 'K LEAGUE Classic',
                  341: 'LIGA Bancomer MX',
                  54: 'Liga Adelante',
                  53: 'Liga BBVA',
                  336: 'Liga Dimayor',
                  308: 'Liga NOS',
                  7: 'Liga do Brasil',
                  16: 'Ligue 1',
                  17: 'Ligue 2',
                  39: 'MLS',
                  353: 'Primera División',
                  4: 'Pro League',
                  189: 'Raiffeisen SL',
                  76: 'Rest of World',
                  67: 'Russian League',
                  65: 'SSE Airtricity Lge',
                  50: 'Scottish Prem',
                  31: 'Serie A TIM',
                  32: 'Serie B',
                  68: 'Süper Lig',
                  41: 'Tippeligaen',
                  80: 'Ö Bundesliga'}

for league in league_numbers.keys():
    
    league_player_data = []
    page = 1
    while True:
        try:
            league_player_data.extend(getlist(league, page))
            print("Added page " + str(page))
            page += 1
        except HTTPError:
            print("Finished " + league_numbers[league] + " (league " + str(league) + ")")
            break
            
            
#    print(league_player_data)
    print("Num players: " + str(len(league_player_data)))
            
    ofile = open('players_league_' + league_numbers[league] + '.csv', 'wb')
    writer = csv.writer(ofile)
    
    writer.writerow(['Name', 'Nationality', 'Overall', 'Potential', 'Position', 'Age', 'Team'])
#    print(league_player_data[0])
#    writer.writerow(league_player_data[0])
#    ofile.close()
    for player in league_player_data:
        writer.writerow(player)
        
    ofile.close()
    print("Written csv file for league " + str(league))
            
            
            
            